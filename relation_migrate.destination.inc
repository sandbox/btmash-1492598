<?php

class MigrateDestinationRelation extends MigrateDestinationEntity {
  static public function getKeySchema() {
    return array(
      'rid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'ID of destination relation',
      ),
    );
  }

  /**
   * Return an options array for relation destinations.
   *
   * @param string $language
   *  Default language for relations created via this destination class.
   * @param string $text_format
   *  Default text format for nodes created via this destination class.
   */
  static public function options($language, $text_format) {
    return compact('language', 'text_format');
  }

  /**
   * Basic initialization
   *
   * @param string $bundle
   *  Relation type of the relation entity.
   * @param array $options
   *  Options applied to relations.
   */
  public function __construct($bundle, array $options = array()) {
    parent::__construct('relation', $bundle, $options);
  }

  /**
   * Returns a list of fields available to be mapped for the relation type (bundle)
   *
   * @return array
   *  Keys: machine names of the fields (to be passed to addFieldMapping)
   *  Values: Human-friendly descriptions of the fields.
   */
  public function fields() {
    $fields = array();
    $fields['rid'] = t('Relation: Existing relation ID');
    $fields['uid'] = t('Relation: Authored by (uid)');
    $fields['created'] = t('Relation: Created timestamp');
    $fields['changed'] = t('Relation: Modified timestamp');

    // Then add in anything provided by handlers
    $fields += migrate_handler_invoke_all('entity', 'fields', $this->entityType, $this->bundle);
    $fields += migrate_handler_invoke_all('relation', 'fields', $this->entityType, $this->bundle);

    return $fields;
  }

  /**
   * Delete a batch of relations at once.
   *
   * @param $rids
   *  Array of relation IDs to be deleted.
   */
  public function bulkRollback(array $rids) {
    migrate_instrument_start('relation_delete_multiple');
    $this->prepareRollback($rids);
    relation_delete_multiple($rids);
    $this->completeRollback($rids);
    migrate_instrument_stop('relation_delete_multiple');
  }

  /**
   * Import a single relation.
   *
   * @param $relation
   *  Relation object to build. Prefilled with any fields mapped in the Migration.
   * @param $row
   *  Raw source data object - passed through to prepare/complete handlers.
   * @return array
   *  Array of key fields (nid only in this case) of the relation that was saved if
   *  successful. FALSE on failure.
   */
  public function import(stdClass $relation, stdClass $row) {
    $migration = Migration::currentMigration();
    if ($migration->getSystemOfRecord() == Migration::SOURCE) {
      if (!isset($relation->language)) {
        $relation->language = $this->language;
      }
      if (!isset($relation->created)) {
        $relation->created = REQUEST_TIME;
      }
      if (!isset($relation->changed)) {
        $relation->changed = REQUEST_TIME;
      }
      if (!isset($relation->is_new)) {
        $relation->changed = REQUEST_TIME;
      }
      if (!isset($relation->uid)) {
        $relation->uid = $GLOBALS['user']->uid;
      }
    }
    $relation->relation_type = $this->bundle;
    // Invoke migration prepare handlers
    $this->prepare($relation, $row);
    if (!isset($relation->revision)) {
      $relation->revision = 0; // Saves disk space and writes. Can be overridden.
    }
    migrate_instrument_start('relation_save');
    relation_save($relation);
    migrate_instrument_stop('relation_save');

    list($id, $vid, $bundle) = entity_extract_ids('relation', $relation);

    if (isset($id)) {
      if ($updating) {
        $this->numUpdated++;
      }
      else {
        $this->numCreated++;
      }

      $return = array($id);
    }
    else {
      $return = FALSE;
    }

    $this->complete($relation, $row);
    return $return;
  }

}
