<?php

class RelationMigrateReferencesD7 extends Migration {
  public function __construct() {
    parent::__construct();
    $this->dependencies = array();
    $this->description = 'Copy the contents from the {references field} table to relation entity';
    $this->map = new MigrateSQLMap($this->machineName,
      array (
        'entity_type' => array (
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ),
        'entity_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationRelation::getKeySchema()
    );

    $query = db_select('field_data_field_related_content', 'f')
              ->fields('f', array('entity_type', 'entity_id', 'field_related_content_nid'));
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationRelation('super_related_articles');
  }

  public function prepare(stdClass $relation, stdClass $source_row) {
    $relation->endpoints[LANGUAGE_NONE] = array(
      array('entity_type' => $source_row->entity_type, 'entity_id' => $source_row->entity_id),
      array('entity_type' => 'node', 'entity_id' => $source_row->field_related_content_nid),
    );
  }
}
